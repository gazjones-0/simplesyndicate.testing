﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Linq;
using System.Linq.Expressions;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Testing.Repositories
{
    /// <summary>
    /// Concrete implementation of <see cref="IRepository{TEntity}"/> that has nothing implemented; every operation will always throw an exception.
    /// </summary>
    /// <remarks>
    /// This class is useful for testing exception-handling code paths that deal with failed connections, etc.
    /// </remarks>
    /// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
    public class NotImplementedRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Gets or sets where Dispose has been called or not.
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotImplementedRepository{TEntity}"/> class.
        /// </summary>
        public NotImplementedRepository()
        {
        }

        /// <overloads>
        /// <summary>
        /// Gets all the entities in the repository.
        /// <note type="important">
        /// It is strongly recommended that <see cref="All{TProjection}"/> be used rather than <see cref="All" /> to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Gets all the entities in the repository.
        /// <note type="important">
        /// As every field of every entity will be returned, it is strongly recommended that <see cref="All{TProjection}"/> be used instead to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// <returns>An <see cref="IQueryable"/> of all the entities.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public IQueryable<TEntity> All() => throw new NotImplementedException();

        /// <summary>
        /// Gets all the entities in the repository, projecting them to type <typeparamref name="TProjection"/>.
        /// <note type="note">
        /// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
        /// </note>
        /// </summary>
        /// <typeparam name="TProjection">Type to project the entities to.</typeparam>
        /// <returns>An <see cref="IQueryable"/> of all the projected entities.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public IQueryable<TProjection> All<TProjection>() => throw new NotImplementedException();

        /// <summary>
        /// Returns the number of entities in the repository.
        /// </summary>
        /// <returns>The number of entities in the repository.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public int Count() => throw new NotImplementedException();

        /// <overloads>
        /// <summary>
        /// Finds an entity.
        /// <note type="important">
        /// It is strongly recommended that <see cref="Find{TProjection}"/> be used rather than <see cref="Find" /> to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Finds an entity with the given primary key. If no entity is found then <c>null</c> is returned.
        /// <note type="important">
        /// As every field of the entity will be returned, it is strongly recommended that <see cref="Find{TProjection}"/> be used instead to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// <remarks>
        /// The <typeparamref name="TEntity"/> must have a member decorated with a <c>KeyAttribute</c>".
        /// </remarks>
        /// <param name="id">Id of the entity to find.</param>
        /// <returns>The entity found, or <c>null</c>.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public TEntity Find(int? id) => throw new NotImplementedException();

        /// <summary>
        /// Finds an entity using the specified predicate, projecting it to <typeparamref name="TProjection"/>. If no entity is found then <c>null</c> is returned.
        /// <note type="note">
        /// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
        /// </note>
        /// </summary>
        /// <remarks>
        /// The <typeparamref name="TEntity"/> must have a member decorated with a <c>KeyAttribute</c>.
        /// </remarks>
        /// <typeparam name="TProjection">Type to project the entity to.</typeparam>
        /// <param name="predicate">Predicate used to find the entity.</param>
        /// <returns>The projected entity found, or <c>null</c>.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public TProjection Find<TProjection>(Expression<Func<TEntity, bool>> predicate) => throw new NotImplementedException();

        /// <summary>
        /// Adds an entity to the repository in such a manner that it will be inserted into the underling data store when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public void Add(TEntity entity) => throw new NotImplementedException();

        /// <summary>
        /// Updates an entity in the repository in such a manner that the underlying data store will be updated when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public void Update(TEntity entity) => throw new NotImplementedException();

        /// <summary>
        /// Marks an entity in the repository in such a manner that it will be deleted from the underling data store when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public void Delete(TEntity entity) => throw new NotImplementedException();

        /// <summary>
        /// Saves all changes made to the repository to the underlying data store.
        /// </summary>
        /// <returns>The number of objects written to the underlying data store.</returns>
        /// <exception cref="NotImplementedException">Always thrown.</exception>
        public int SaveChanges() => throw new NotImplementedException();

        /// <summary>
        /// Releases all resources that are used by the current instance of the <see cref="NotImplementedRepository{TEntity}"/> class.
        /// </summary>
        /// <remarks>
        /// The Dispose method leaves the <see cref="NotImplementedRepository{TEntity}"/> in an unusable state.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and optionally releases managed resources.
        /// </summary>
        /// <remarks>
        /// The Dispose method leaves the <see cref="NotImplementedRepository{TEntity}"/> in an unusable state.
        /// </remarks>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
            }

            _disposed = true;
        }
    }
}
