﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Testing.Repositories
{
    /// <summary>
    /// Concrete implementation of <see cref="IRepository{TEntity}"/> that uses an in-memory data store for the repository with no
    /// permanent data storage.
    /// </summary>
    /// <remarks>
    /// This class is designed to act as a stand-in during unit-testing, and avoids the need to have a separate data store (e.g. a database server).
    /// </remarks>
    /// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
    public class InMemoryRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        /// <summary>
        /// Gets or sets where Dispose has been called or not.
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Acts as our data store for the repository.
        /// </summary>
        private IList<TEntity> _list;

        /// <summary>
        /// Simple count of the number of changes to provide a rough simulation for SaveChanges.
        /// </summary>
        private int _changeCount = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="InMemoryRepository{TEntity}"/> class.
        /// </summary>
        public InMemoryRepository() => _list = new List<TEntity>();

        /// <overloads>
        /// <summary>
        /// Gets all the entities in the repository.
        /// <note type="important">
        /// It is strongly recommended that <see cref="All{TProjection}"/> be used rather than <see cref="All" /> to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Gets all the entities in the repository.
        /// <note type="important">
        /// As every field of every entity will be returned, it is strongly recommended that <see cref="All{TProjection}"/> be used instead to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// <returns>An <see cref="IQueryable"/> of all the entities.</returns>
        public IQueryable<TEntity> All() => _list.AsQueryable<TEntity>();

        /// <summary>
        /// Gets all the entities in the repository, projecting them to type <typeparamref name="TProjection"/>.
        /// <note type="note">
        /// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
        /// </note>
        /// </summary>
        /// <typeparam name="TProjection">Type to project the entities to.</typeparam>
        /// <returns>An <see cref="IQueryable"/> of all the projected entities.</returns>
        public IQueryable<TProjection> All<TProjection>() => _list.AsQueryable<TEntity>().ProjectTo<TProjection>();

        /// <summary>
        /// Returns the number of entities in the repository.
        /// </summary>
        /// <returns>The number of entities in the repository.</returns>
        public int Count() => _list.Count;

        /// <overloads>
        /// <summary>
        /// Finds an entity.
        /// <note type="important">
        /// It is strongly recommended that <see cref="Find{TProjection}"/> be used rather than <see cref="Find" /> to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Finds an entity with the given primary key. If no entity is found then <c>null</c> is returned.
        /// <note type="important">
        /// As every field of the entity will be returned, it is strongly recommended that <see cref="Find{TProjection}"/> be used instead to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// <remarks>
        /// The <typeparamref name="TEntity"/> must have a member decorated with a <see cref="System.ComponentModel.DataAnnotations.KeyAttribute"/>.
        /// </remarks>
        /// <param name="id">Id of the entity to find.</param>
        /// <returns>The entity found, or <c>null</c>.</returns>
        public TEntity Find(int? id)
        {
            var results = _list.Where(x => GetKeyValue(x) == id);
            if (results.Count() > 0)
            {
                return results.First();
            }

            return null;
        }

        /// <summary>
        /// Finds an entity using the specified predicate, projecting it to <typeparamref name="TProjection"/>. If no entity is found then <c>null</c> is returned.
        /// <note type="note">
        /// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
        /// </note>
        /// </summary>
        /// <remarks>
        /// The <typeparamref name="TEntity"/> must have a member decorated with a <see cref="System.ComponentModel.DataAnnotations.KeyAttribute"/>.
        /// </remarks>
        /// <typeparam name="TProjection">Type to project the entity to.</typeparam>
        /// <param name="predicate">Predicate used to find the entity.</param>
        /// <returns>The projected entity found, or <c>null</c>.</returns>
        public TProjection Find<TProjection>(Expression<Func<TEntity, bool>> predicate) => _list.AsQueryable<TEntity>().Where(predicate).ProjectTo<TProjection>().FirstOrDefault();

        /// <summary>
        /// Updates an entity in the repository in such a manner that the underlying data store will be updated when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        public void Add(TEntity entity)
        {
            _list.Add(entity);
            _changeCount++;
        }

        /// <summary>
        /// Marks an entity in the repository in such a manner that it will be deleted from the underling data store when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        public void Update(TEntity entity)
        {
            foreach (var item in _list)
            {
                if (GetKeyValue(item) == GetKeyValue(entity))
                {
                    _list.Remove(item);
                    _list.Add(entity);
                    _changeCount++;
                    return;
                }
            }

            throw new InvalidOperationException();
        }

        /// <summary>
        /// Deletes an entity from the repository; use <see cref="SaveChanges"/> to permanently save the change to the underlying data store the repository uses.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        public void Delete(TEntity entity)
        {
            _list.Remove(entity);
            _changeCount++;
        }

        /// <summary>
        /// Saves all changes made to the repository to the underlying data store.
        /// </summary>
        /// <returns>The number of objects written to the underlying data store.</returns>
        public int SaveChanges()
        {
            var curChangeCount = _changeCount;
            _changeCount = 0;
            return curChangeCount;
        }

        /// <summary>
        /// Releases all resources that are used by the current instance of the <see cref="InMemoryRepository{TEntity}"/> class.
        /// </summary>
        /// <remarks>
        /// The Dispose method leaves the <see cref="InMemoryRepository{TEntity}"/> in an unusable state.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and optionally releases managed resources.
        /// </summary>
        /// <remarks>
        /// The Dispose method leaves the <see cref="InMemoryRepository{TEntity}"/> in an unusable state.
        /// </remarks>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _list = null;
            }

            _disposed = true;
        }

        /// <summary>
        /// Gets the value of the key on the specified <paramref name="entity"/>.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>The value of the <paramref name="entity"/>'s key.</returns>
        private static int? GetKeyValue(TEntity entity) => AttributeHelpers.GetAttributeValue<int?>(entity, typeof(System.ComponentModel.DataAnnotations.KeyAttribute));
    }
}
