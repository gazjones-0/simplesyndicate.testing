﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.ComponentModel.DataAnnotations;

namespace SimpleSyndicate.Testing
{
    /// <summary>
    /// Test item that can be used in collections, etc.
    /// </summary>
    public class TestItem
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>Id.</value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value.
        /// </summary>
        /// <value>Integer value.</value>
        public int IntValue { get; set; }
    }
}
