﻿using System.Linq;
using AutoMapper;
using SimpleSyndicate.Testing.Repositories;
using Xunit;

namespace SimpleSyndicate.Testing.Tests.Repositories
{
    public class InMemoryRepositoryTests
    {
        private void InitializeAutoMapper()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg => cfg.CreateMap<TestItem, MappedTestItem>());
        }

        [Fact]
        public void All()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem());
            repository.Add(new TestItem());

            // act
            var query = repository.All();

            // assert
            Assert.Equal(2, query.Count());
        }

        [Fact]
        public void AllTemplated()
        {
            // arrange
            InitializeAutoMapper();
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem());
            repository.Add(new TestItem());

            // act
            var query = repository.All<MappedTestItem>();

            // assert
            Assert.Equal(2, query.Count());
        }

        [Fact]
        public void Count()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem());
            repository.Add(new TestItem());

            // act
            var count = repository.Count();

            // assert
            Assert.Equal(2, count);
        }

        [Fact]
        public void Find()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });
            repository.Add(new TestItem() { Id = 2 });

            // act
            var item = repository.Find(1);

            // assert
            Assert.NotNull(item);
        }

        [Fact]
        public void FindIsNull()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });
            repository.Add(new TestItem() { Id = 2 });

            // act
            var item = repository.Find(null);

            // assert
            Assert.Null(item);
        }

        [Fact]
        public void FindTemplated()
        {
            // arrange
            InitializeAutoMapper();
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });
            repository.Add(new TestItem() { Id = 2 });

            // act
            var item = repository.Find<MappedTestItem>(x => x.Id == 1);

            // assert
            Assert.NotNull(item);
        }

        [Fact]
        public void FindTemplatedIsNull()
        {
            // arrange
            InitializeAutoMapper();
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });
            repository.Add(new TestItem() { Id = 2 });

            // act
            var item = repository.Find<MappedTestItem>(x => x.Id == -1);

            // assert
            Assert.Null(item);
        }

        [Fact]
        public void Add()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });
            repository.Add(new TestItem() { Id = 2 });

            // act
            var count = repository.Count();

            // assert
            Assert.Equal(2, count);
        }

        [Fact]
        public void Update()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1, IntValue = 1 });
            var item = new TestItem() { Id = 2, IntValue = 2 };
            repository.Add(item);

            // act
            item.IntValue = 3;
            repository.Update(item);

            // assert
            Assert.Equal(0, repository.All().Where(x => x.IntValue == 2).Count());
            Assert.Equal(1, repository.All().Where(x => x.IntValue == 3).Count());
        }

        [Fact]
        public void Delete()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });
            var item = new TestItem() { Id = 2 };
            repository.Add(item);
            var count = repository.Count();

            // act
            repository.Delete(item);

            // assert
            Assert.Equal(count - 1, repository.Count());
        }

        [Fact]
        public void SaveChanges()
        {
            // arrange
            var repository = new InMemoryRepository<TestItem>();
            repository.Add(new TestItem() { Id = 1 });

            // act
            var saveChanges = repository.SaveChanges();

            // assert
            Assert.Equal(1, saveChanges);
        }
    }
}
