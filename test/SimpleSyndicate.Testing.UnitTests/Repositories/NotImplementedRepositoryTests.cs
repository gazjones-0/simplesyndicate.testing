﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using SimpleSyndicate.Testing.Repositories;
using Xunit;

namespace SimpleSyndicate.Testing.Tests.Repositories
{
    public class NotImplementedRepositoryTests
    {
        [Fact]
        public void AllThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.All(); };
            Assert.Throws<NotImplementedException>((Action) act);
        }

        [Fact]
        public void AllTemplatedThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.All<object>(); }
            Assert.Throws<NotImplementedException>((Action) act);
        }

        [Fact]
        public void CountThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.Count(); }
            Assert.Throws<NotImplementedException>((Action) act);
        }

        [Fact]
        public void FindThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.Find(null); }
            Assert.Throws<NotImplementedException>((Action)act);
        }

        [Fact]
        public void FindTemplatedThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.Find<object>(null); }
            Assert.Throws<NotImplementedException>((Action)act);
        }

        [Fact]
        public void AddThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.Add(null); }
            Assert.Throws<NotImplementedException>((Action)act);
        }

        [Fact]
        public void UpdateThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.Update(null); }
            Assert.Throws<NotImplementedException>((Action)act);
        }

        [Fact]
        public void DeleteThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.Delete(null); }
            Assert.Throws<NotImplementedException>((Action)act);
        }

        [Fact]
        public void SaveChangesThrowsException()
        {
            var repository = new NotImplementedRepository<object>();
            void act() { repository.SaveChanges(); }
            Assert.Throws<NotImplementedException>((Action)act);
        }
    }
}
